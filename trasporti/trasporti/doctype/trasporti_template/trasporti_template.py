# -*- coding: utf-8 -*-
# Copyright (c) 2015, Fayez Qandeel and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class TrasportiTemplate(Document):
	pass

@frappe.whitelist()
def get_template(template=None):
	return frappe.get_doc("Sales Order Template",template)