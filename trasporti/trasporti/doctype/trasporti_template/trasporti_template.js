// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt



frappe.intervals_manager_trasporti = false;
$(window).on('hashchange', function() {
    frappe.intervals_manager_trasporti.clearAll();
    //console.log(frappe.intervals_manager);
    if (window.location.hash.indexOf("Trasporti%20Template/") == -1 && window.location.hash.indexOf("Trasporti Template/") == -1) {
        $(".layout-side-section").show().next().removeClass("col-md-12").addClass("col-md-10");
        window.full_screen_mode = false;
        $('.container').removeClass("trasporti_container");
        $('.fullscreen_btn_').text("Full Screen");
        window.rows_editor.frm.page.wrapper.find('.menu-btn-group').show();

    } else {

        frappe.intervals_manager_trasporti.rebuild();
        window.rows_editor.frm.page.wrapper.find('.menu-btn-group').hide();
        $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
    }
});

// frappe.require("assets/frappe/js/lib/socket.io.min.js");
// frappe.require("assets/frappe/js/frappe/socketio_client.js");
// frappe.socket.init();





frappe.ui.form.on('Trasporti Template', {
    setup: function(frm) {

        setTimeout(function() {
            if (frappe.socket) {
                if(window.rows_editor && window.rows_editor.frm.doc.ravenna_sales_order){
                  frappe.socket.socket.emit('doc_subscribe', "Sales Order Template",window.rows_editor.frm.doc.ravenna_sales_order);
                }
                frappe.socket.socket.on('doc_update', function(data) {
                    if (data.doctype == "Sales Order Template" && window.rows_editor && data.name == window.rows_editor.frm.doc.ravenna_sales_order) {
                        console.log("realtime2", window.rows_editor.frm.doc.ravenna_sales_order);
                        window.rows_editor.force_update();
                    }
                });
                var modified = {};
                frappe.socket.socket.on('dirottamenti_updates', function(data) {
                    if(frm.doc.ravenna_sales_order==data.ravenna_sales_order && window.rows_editor){
                      for(var i in window.rows_editor.template_data.template_custom_items){
                        var item =  window.rows_editor.template_data.template_custom_items[i];
                        for(var j in data.dirottamenti_template_details){
                          var subitem = data.dirottamenti_template_details[j];

                          if(item.customer == subitem.customer && item.item==subitem.item && item.quantity != subitem.quantity){
                            modified[item.customer] = data.name;
                          }
                        }
                      }
                      console.log(modified);
                      for(var i in frm.doc.trasporti_template_details){
                        var item = frm.doc.trasporti_template_details[i];
                        if(modified[item.customer]){
                          item.dirottato = modified[item.customer];
                        }
                      }
                      frm.refresh_field("trasporti_template_details");
                      frm.timeline.insert_comment("Workflow", " - "+data.user+" did live changes on trasporti values");
                      frm.timeline.refresh();
                      //frm.save();
                      modified = {};
                    }
                });
            }
        }, 0);
        frm.page.clear_inner_toolbar();
        frappe.intervals_manager_trasporti = new frappe.intervals_manager_trasporti(frm);
        $(function() {
            frm.page.wrapper.find('.menu-btn-group').hide()
            $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
            if ($('.fullscreen_btn_').length == 0) {
                $('.page-actions').find('.menu-btn-group').before('<span style="margin-right:5px;" class="fullscreen_btn_ btn btn-secondary btn-default btn-sm">Full Screen</span>');
            }
            window.full_screen_mode = false;
            $('.fullscreen_btn_').on('click', function() {


                if (!window.full_screen_mode) {
                    $(this).text("Close Full Screen");
                    window.full_screen_mode = true;
                } else {
                    $(this).text("Full Screen");
                    window.full_screen_mode = false;
                }
                $('.container').toggleClass("trasporti_container");
            });
        });


    },
    validate: function(frm) {
        frm.timeline.insert_comment("Workflow", "- saved the template");
        frm.timeline.refresh();
    },
    refresh: function(frm) {
        var me = this;
        $("body").addClass("trasporti_template");
        $(".layout-side-section").hide().next().removeClass("col-md-10").addClass("col-md-12");
        $("body").on("grid-rendered", function(event, grid_obj) {
            if (window.location.hash.indexOf("Trasporti%20Template/") != -1 || window.location.hash.indexOf("Trasporti Template/") != -1) {
                $(grid_obj.wrapper).find(".grid-footer").remove();
                window.rows_editor = new frappe.RowsEditor(frm, grid_obj);
            }

        });
        $("body").on("grid-row-rendered", function(event, grid_row_obj) {
            if (window.location.hash.indexOf("Trasporti%20Template/") != -1 || window.location.hash.indexOf("Trasporti Template/") != -1) {
                $(grid_row_obj.wrapper).find(".row-index,.btn-open-row").remove();
            }
        });
    },
    ravenna_sales_order: function(frm) {
      if(frm.doc.ravenna_sales_order){
          frappe.socket.socket.emit('doc_subscribe', "Sales Order Template",frm.doc.ravenna_sales_order);
        }
        window.rows_editor.fetch_customers();
    }
});

frappe.intervals_manager_trasporti = Class.extend({
    init: function(frm) {

        this.frm = frm;
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Trasporti Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    rebuild: function() {
        var me = this;
        if (!this.intervals_array) {
            this.intervals_array = setInterval(function() {
                if (me.frm.doctype == "Trasporti Template") {
                    me.frm.save();
                    me.frm.timeline.insert_comment("Workflow", "- auto saved the template");
                    me.frm.timeline.refresh();
                }
            }, 60000);
        }
    },
    clearAll: function() {
        clearInterval(this.intervals_array);
        this.intervals_array = false;
    }
});


frappe.RowsEditor = Class.extend({
    init: function(frm, grid_obj) {
        this.frm = frm;
        this.grid_obj = grid_obj;
        this.fetch_customers(frm);
    },
    force_update: function() {
        console.log("realtime", this.frm.doc.ravenna_sales_order);
        this.fetch_customers();
    },
    fetch_customers: function() {

        function compare(a,b) {
          if (a.atb< b.atb)
            return -1;
          if (a.atb > b.atb)
            return 1;
          return 0;
        }

        

        var me = this;
        var current_customers = [];
        for (var i in me.frm.doc.trasporti_template_details) {
            current_customers.push(me.frm.doc.trasporti_template_details[i].customer);
        }
        if (typeof me.frm.doc.ravenna_sales_order != "undefined") {
            frappe.call({
                method: 'trasporti.trasporti.doctype.trasporti_template.trasporti_template.get_template',
                args: {
                    template: me.frm.doc.ravenna_sales_order
                },
                callback: function(r) {
                    var new_customers = [];
                    for (var i in r.message.template_custom_items) {
                        new_customers.push(r.message.template_custom_items[i].customer);
                    }
                    console.log(current_customers, new_customers, current_customers !== new_customers);
                    console.log(me);

                    function get_old_item(details,orari,customer,atb,warehouse,supplier,qty){
                      var found = false;
                      for(var i in details){
                          var item = details[i];
                          if(item.orari == orari && item.customer == customer){
                            found = true;
                            return item;
                          }
                      }
                      if(!found){
                        return {"orari":orari,"customer":customer,"atb":atb,warehouse,supplier,"qty":qty};
                      }
                    }

                    //if (current_customers.toString() !== new_customers.toString()) {
                    me.template_data = r.message;
                    var details = me.frm.doc.trasporti_template_details;
                    me.frm.doc.trasporti_template_details = [];
                    var trasporti_template_details = [];
                    for (var i in me.template_data.template_custom_items) {
                        var new_item = get_old_item(details,me.template_data.template_custom_items[i].carrier_shipping,me.template_data.template_custom_items[i].customer,me.template_data.template_custom_items[i].atb,me.template_data.template_custom_items[i].warehouse,me.template_data.template_custom_items[i].supplier,
                            me.template_data.template_custom_items[i].summary_qty
                            );
                        //console.log(me.template_data.template_custom_items[i].orari);
                        trasporti_template_details.push(new_item);
                    }

                    me.frm.doc.trasporti_template_details = trasporti_template_details.sort(compare);
                    me.frm.save();
                    //}

                    console.log(me.frm.doc.trasporti_template_details);
                }
            });
        }



    }
});

// frappe.ui.form.GridRowForm = Class.extend({
//  init: function(opts) {
//    $.extend(this, opts);
//    this.wrapper = $('<div class="form-in-grid"></div>')
//      .appendTo(this.row.wrapper);

//  },
//  render: function() {
//    var me = this;
//    this.make_form();
//    this.form_area.empty();

//    this.layout = new frappe.ui.form.Layout({
//      fields: this.row.docfields,
//      body: this.form_area,
//      no_submit_on_enter: true,
//      frm: this.row.frm,
//    });
//    this.layout.make();

//    this.fields = this.layout.fields;
//    this.fields_dict = this.layout.fields_dict;

//    this.layout.refresh(this.row.doc);

//    // copy get_query to fields
//    for(var fieldname in (this.row.grid.fieldinfo || {})) {
//      var fi = this.row.grid.fieldinfo[fieldname];
//      $.extend(me.fields_dict[fieldname], fi);
//    }

//    this.toggle_add_delete_button_display(this.wrapper);

//    this.row.grid.open_grid_row = this;

//    this.set_focus();
//  },
//  make_form: function() {
//    if(!this.form_area) {
//      $(frappe.render_template("grid_form", {grid:this})).appendTo(this.wrapper);
//      this.form_area = this.wrapper.find(".form-area");
//      this.row.set_row_index();
//      this.set_form_events();
//    }
//  },
//  set_form_events: function() {
//    var me = this;
//    this.wrapper.find(".grid-delete-row")
//      .on('click', function() {
//        me.row.remove(); return false;
//      });
//    this.wrapper.find(".grid-insert-row")
//      .on('click', function() {
//        me.row.insert(true); return false;
//      });
//    this.wrapper.find(".grid-insert-row-below")
//      .on('click', function() {
//        me.row.insert(true, true); return false;
//      });
//    this.wrapper.find(".grid-append-row")
//      .on('click', function() {
//        me.row.toggle_view(false);
//        me.row.grid.add_new_row(me.row.doc.idx+1, null, true);
//        return false;
//      });
//    this.wrapper.find(".grid-form-heading, .grid-footer-toolbar").on("click", function() {
//      me.row.toggle_view();
//      return false;
//    });
//  },
//  toggle_add_delete_button_display: function($parent) {
//    $parent.find(".grid-header-toolbar .btn, .grid-footer-toolbar .btn")
//      .toggle(this.row.grid.is_editable());
//  },
//  refresh_field: function(fieldname) {
//    if(this.fields_dict[fieldname]) {
//      this.fields_dict[fieldname].refresh();
//      this.layout && this.layout.refresh_dependency();
//    }
//  },
//  set_focus: function() {
//    // wait for animation and then focus on the first row
//    var me = this;
//    setTimeout(function() {
//      if(me.row.frm.doc.docstatus===0) {
//        var first = me.form_area.find("input:first");
//        if(first.length && !in_list(["Date", "Datetime", "Time"], first.attr("data-fieldtype"))) {
//          try {
//            first.get(0).focus();
//          } catch(e) {
//            //
//          }
//        }
//      }
//    }, 500);
//  },
// });
