# Copyright (c) 2013, Fayez Qandeel and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	columns, data = [], get_data()

	columns.append({
			"label": _("Ravenna Template"),
			"fieldname": "ravenna_sales_order",
			"fieldtype": "Link",
			"options":"Sales Order Template",
			"width": 120
		})
	columns.append({
			"label": _("Customer"),
			"fieldname": "customer",
			"fieldtype": "Link",
			"options":"Customer",
			"width": 120
		})
	columns.append({
			"label": _("Orari"),
			"fieldname": "orari",
			"fieldtype": "data",
			"width": 120
		})
	columns.append({
			"label": _("Botte"),
			"fieldname": "data",
			"fieldtype": "botte",
			"width": 120
		})
	columns.append({
			"label": _("Autista"),
			"fieldname": "autista",
			"fieldtype": "autista",
			"width": 120
		})
	columns.append({
			"label": _("ATB"),
			"fieldname": "atb",
			"fieldtype": "data",
			"width": 120
		})
	columns.append({
			"label": _("Dirottato"),
			"fieldname": "dirottato",
			"fieldtype": "Link",
			"options":"Dirottamenti Template",
			"width": 120
		})
	columns.append({
			"label": _("Magazzino"),
			"fieldname": "warehouse",
			"fieldtype": "Link",
			"options":"Warehouse",
			"width": 120
		})
	columns.append({
			"label": _("Fornitore"),
			"fieldname": "supplier",
			"fieldtype": "Link",
			"options":"Supplier",
			"width": 120
		})
	columns.append({
			"label": _("QTY"),
			"fieldname": "qty",
			"fieldtype": "Int",
			"width": 120
		})
	return columns, data
def get_data():
	data = []
	
	data = frappe.db.sql("""select 
			ttd.name as id,
			ttd.parent as ravenna_sales_order,
			ttd.customer as customer,
			ttd.orari as orari,
			ttd.botte as botte,
			ttd.autista as autista,
			ttd.atb as atb,
			ttd.dirottato as dirottato,
			ttd.warehouse as warehouse,
			ttd.supplier as supplier,
			ttd.qty as qty
			from
			 `tabTrasporti Template Details` ttd
				
			 """ ,as_dict=1)
	return data