from __future__ import unicode_literals

import frappe
from frappe.async import publish_realtime

@frappe.whitelist()
def template_updated(doc,method):
    message = {}
	message['broadcast'] = True
    print message,"realtimetest"
    frappe.publish_realtime('new_message', message, after_commit=True)
